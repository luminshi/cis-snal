#SNAL Report 2

## Reading
Read this [Overview of the Linux filesystem](http://tldp.org/LDP/intro-linux/html/sect_03_01.html)


## Disk partitions
For each system, how many disks do you see? How can you tell which ones are local and which ones are mounted over the network? Briefly describe similarities and differences between system configurations in your report.

* Virtual machine

	I installed linux mint dist. on my own tower, and here's partial output from the terminal

	    Filesystem      Size  Used Avail Use% Mounted on
	    /dev/sda1        38G   22G   14G  61% /
	    none            4.0K     0  4.0K   0% /sys/fs/cgroup
	    udev            979M  4.0K  979M   1% /dev
	    tmpfs           199M  1.3M  198M   1% /run
	    none            5.0M     0  5.0M   0% /run/lock
	    none            994M   76K  994M   1% /run/shm
	    none            100M   52K  100M   1% /run/user
	    .host:/         932G   94G  839G  11% /mnt/hgfs

    As you can see, only one 2 actual disk been mounted on the system. The local disk is named **/dev/sda1**, and the remote disk is named as **.host:/** (my shared folder in VM)
    
* The Raspberry Pi

	We have two usb sticks installed when we ran the ```df -h``` command, and here's the out put.

		Filesystem      Size  Used Avail Use% Mounted on
		rootfs           28G  2.5G   24G  10% /
		/dev/root        28G  2.5G   24G  10% /
		devtmpfs        460M     0  460M   0% /dev
		.....
		/dev/sda1       7.3G  4.0K  7.3G   1% /media/KINGSTON
		/dev/mmcblk0p5  488M  408K  452M   1% /media/data
		/dev/mmcblk0p3   27M  397K   25M   2% /media/SETTINGS
		/dev/sdb1       7.3G  4.0K  7.3G   1% /media/KINGSTON_
	
	We have 3 local disks here in this case
* ix-trusty

	Here's the output from ix server:
	
		Filesystem                                        Size  Used Avail Use% Mounted on
	    /dev/vda1                                          24G   14G  8.6G  62% /
	    none                                              4.0K     0  4.0K   0% /sys/fs/cgroup
	    udev                                               16G  4.0K   16G   1% /dev
	    tmpfs                                             3.2G  596K  3.2G   1% /run
	    none                                              5.0M     0  5.0M   0% /run/lock
	    none                                               16G     0   16G   0% /run/shm
	    none                                              100M  4.0K  100M   1% /run/user
	    cranium.cs.uoregon.edu:/p/web/weblogs              24T   13G   24T   1% /cs/weblogs
	    cranium.cs.uoregon.edu:/p/local/linux              24T  2.0G   24T   1% /local
	    cranium.cs.uoregon.edu:/home/users/yuegu          2.0G   42M  2.0G   3% /home/users/yuegu
	    cranium.cs.uoregon.edu:/home/users/byeganeh       2.0G  1.6G  446M  79% /home/users/byeganeh
	    ....
	    ....
	    ....

    There are many mounted disks including one local disks and many remote disks, and all the remote disks beginning with ```cranium.cs.uoregon.edu:/```

* MacBook

	Here's the result from my mac laptop
	
	    Filesystem      Size   Used  Avail Capacity  iused    ifree %iused  Mounted on
	    /dev/disk1     233Gi  121Gi  112Gi    52% 31685246 29293568   52%   /
	    devfs          183Ki  183Ki    0Bi   100%      632        0  100%   /dev
	    map -hosts       0Bi    0Bi    0Bi   100%        0        0  100%   /net
	    map auto_home    0Bi    0Bi    0Bi   100%        0        0  100%   /home

    Only one local disk is mounted on my laptop which is named as ```/dev/disk1```



## Expanding disk capacity
* Refer to the [Linux HOWTO](http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch27_:_Expanding_Disk_Capacity#.VR2yL2TF9FI) document for this

* Connect the provided USB drive to your Pi or virtual machine and configure two new partitions on it, called ```/special``` and ```/data```.
	
	* After I created two partitions by using the ```fdisk``` tool, I was able to use ```mkfs``` tool to format the two partitions that I created.
	
	* I then created two folders at ```/```, ```special``` and ```data```.
	
* Mount these partitions (interactively on the command line)
	
	* I used ```sudo mount /dev/sdb1 special``` and ```sudo mount /dev/sdb2 data``` to mount the two partitions to these two folders.
	
	* Here is the result:
	
			pi@duckpi02 / $ df -h
			Filesystem      Size  Used Avail Use% Mounted on
			rootfs           28G  2.5G   24G  10% /
			/dev/root        28G  2.5G   24G  10% /
			devtmpfs        460M     0  460M   0% /dev
			tmpfs            93M  292K   93M   1% /run
			tmpfs           5.0M     0  5.0M   0% /run/lock
			tmpfs           186M     0  186M   0% /run/shm
			/dev/mmcblk0p6   60M   15M   45M  25% /boot
			/dev/mmcblk0p5  488M  408K  452M   1% /media/data
			/dev/mmcblk0p3   27M  397K   25M   2% /media/SETTINGS
			/dev/sdb1       3.3G  7.0M  3.2G   1% /special
			/dev/sdb2       3.7G  7.8M  3.5G   1% /data

* Unmount and eject the USB drive
	* I used ```sudo umount /dev/sdb1``` and ```sudo umount /dev/sdb2``` to unmount the partitions


* Reinsert it and mount the partitions again
	* I basically ran the same cmds to mount the partitions with no problems

* Modify ```/etc/fstab``` and reboot your machine without removing the USB drive; verify that the new USB partitions are mounted correctly.

	* I add the following lines to the fstab file:

			# lumin's usb drive
			/dev/sdb1  /special ext3  defaults 1 2
			/dev/sdb2  /data ext3  defaults 1 2

	
	* pi is able to mount both ```sdb1``` and ```sdb2``` automatically after I rebooted the system.

* Make a subdirectory in the /data partition and place some files there (text or pictures, up to you), and verify that ```unmount/eject/``` remount works and the files are still there.

	* I created an empty file ```test``` and then I unmount the usb drive
	
	* Here's the result:
	
			pi@duckpi02 /special $ sudo touch test
			pi@duckpi02 /special $ ls
			lost+found  test
			pi@duckpi02 / $ sudo umount special
			pi@duckpi02 / $ cd special
			pi@duckpi02 /special $ ls
		
	* As we can see here, there's no more file called test under the special folder


## Disk space problems

* On your Pi, mostly fill up /dev/root partition (e.g., by creating multiple copies of a very large file; ```dd``` and ```yes``` are two other commands that can "help" with that). What happens when you approach 100% use? For example, try creating a new file, looking up help info for some commands, or doing any other command-line activities.

	* I used dd tool to fill up the micro sd card on pi (get content from ```/dev/zero```)
	* Once the available space is close to 0%, there's no option for me to create new file on the sd card.

* Make various subdirectories and spread files of different sizes among them. Then, preferably on another team's Pi, find out the most recently created, most space-consuming directories (and files). If you are doing this outside class, then work in pairs and have one person "hide" the large files and the other team member "find" them, then swap and repeat.

	* after i ran the cmd ```du --time -h | sort -h -r``` in the terminal, I was able to list all the folders in the current folder I am in, by size and last modified time in descending order. 

* Suppose those big files you added are really necessary and you cannot just delete them. How would you fix the problem?

	* you can move the big files to other storage device, and then you can simply make soft link that points to the file you wanted (if that makes life easier)

* Write a program that opens and writes to a file (but does not close it and keeps running). Before you run the program, check the current directory size (e.g., ```du -sk```.) and record that number. Then, delete the file manually from the file system while your program is still writing to it. You can also do this by manually editing a file, but make sure to add substantial amounts of text. Discuss what happens. Can you see the file with ```ls```? What about ```lsof```? Even more advanced: use ```syslog``` to create a system log file and keep writing to it after deleting from file system.
	
	* for this task, i made a little python script as follow:

			import time
			wf = open("workfile.txt", "w")
			while(True):
			        wf.write('jkfldsfdsafdsafdsafewadfdsafdsafdsafdsafdsa' * 100)
			        time.sleep(1)
			wf.close()

	* the little script will write to the workfile.txt file every second.

	* i can see the folder size's increasing by calling ```du -sk``` before i remove the workfile.txt, however, once i deleted the txt file, the folder size rolls back to the initial stage.

	* I can still see the python is writing to the system by using the ```lsof``` tool, and it tags the workfile.txt as ```(deleted)```



## Find a few examples of filesystem objects
Find a few examples of filesystem objects that are not datafiles on your system(s) and describe what they are briefly in your report

* filesystem objects can be anything from a regular file to a block device

* when you do ```ls -l```, you the very first letter of each line represents what the file type is

* for example, ```-``` means a regular file, ```d``` means a directory, ```s``` means a socket (send data via a socket), ```l``` means the file is a link to the other file (soft or hard link)

## RAID disks
* For this exercise, it is best if you use two USB sticks -- team up with someone to work on each Pi or use an extra USB drive of your own.

* Reading: [RAID HOWTO](http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch26_:_Linux_Software_RAID#.VR2_2GTF9FI)

* You may need to install the mdadm package; do not change default options during installation.

* Create a RAID 0 set; include the results of ```cat /proc/mdstat```, ```mdadm --detail --scan``` and the contents of ```/etc/fstab``` in your report before you do the next step.

* Remove the RAID 0 set and create a RAID 1 set, include the same information about that as you did for RAID 0 in your report.


We deleted partitions from both usb drives first. We then created one partition for each of the usb drive and set their system type to Linux raid auto.

		Command (m for help): t
		Selected partition 1
		Hex code (type L to list codes): L
		 
		 0  Empty           24  NEC DOS         81  Minix / old Lin bf  Solaris
		 1  FAT12           27  Hidden NTFS Win 82  Linux swap / So c1  DRDOS/sec (FAT-
		 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
		 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden C:  c6  DRDOS/sec (FAT-
		 4  FAT16 <32M      40  Venix 80286     85  Linux extended  c7  Syrinx
		 5  Extended        41  PPC PReP Boot   86  NTFS volume set da  Non-FS data
		 6  FAT16           42  SFS             87  NTFS volume set db  CP/M / CTOS / .
		 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Dell Utility
		 8  AIX             4e  QNX4.x 2nd part 8e  Linux LVM       df  BootIt
		 9  AIX bootable    4f  QNX4.x 3rd part 93  Amoeba          e1  DOS access
		 a  OS/2 Boot Manag 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
		 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
		 c  W95 FAT32 (LBA) 52  CP/M            a0  IBM Thinkpad hi eb  BeOS fs
		 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         ee  GPT
		 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ef  EFI (FAT-12/16/
		10  OPUS            55  EZ-Drive        a7  NeXTSTEP        f0  Linux/PA-RISC b
		11  Hidden FAT12    56  Golden Bow      a8  Darwin UFS      f1  SpeedStor
		12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f4  SpeedStor
		14  Hidden FAT16 <3 61  SpeedStor       ab  Darwin boot     f2  DOS secondary
		16  Hidden FAT16    63  GNU HURD or Sys af  HFS / HFS+      fb  VMware VMFS
		17  Hidden HPFS/NTF 64  Novell Netware  b7  BSDI fs         fc  VMware VMKCORE
		18  AST SmartSleep  65  Novell Netware  b8  BSDI swap       fd  Linux raid auto
		1b  Hidden W95 FAT3 70  DiskSecure Mult bb  Boot Wizard hid fe  LANstep
		1c  Hidden W95 FAT3 75  PC/IX           be  Solaris boot    ff  BBT
		1e  Hidden W95 FAT1 80  Old Minix
		Hex code (type L to list codes): fd
		Changed system type of partition 1 to fd (Linux raid autodetect)

We then create a raid 0 array by using this cmd: ```mdadm --create --verbose /dev/md/snalraid0 --level=0 --raid-devices=2 /dev/sda1 /dev/sdb1```, we got the result as follow:

		mdadm: chunk size defaults to 512K
		mdadm: /dev/sda1 appears to contain an ext2fs file system
		    size=5242880K  mtime=Wed Apr  8 23:02:39 2015
		mdadm: /dev/sdb1 appears to contain an ext2fs file system
		    size=3573280K  mtime=Thu Apr  9 17:53:40 2015
		Continue creating array? y
		mdadm: Defaulting to version 1.2 metadata
		mdadm: array /dev/md/snalraid0 started.

We then do ```cat /proc/mdstat```:
		Personalities : [raid0]
		md127 : active raid0 sdb1[1] sda1[0]
		      15145984 blocks super 1.2 512k chunks
		 
		unused devices: <none>

And ```mdadm --detail --scan```:

		ARRAY /dev/md/snalraid0 metadata=1.2 name=duckpi02:snalraid0 UUID=5b49f176:6ee53d57:c4becbec:cf0b7f3b

We format the newly created raid0 mount point to ```ext4``` format, ```mkfs.ext4 /dev/md/snalraid0```

We have our fstab sets to this:

		proc            /proc           proc    defaults          0       0
		/dev/mmcblk0p6  /boot           vfat    defaults          0       2
		/dev/mmcblk0p7  /               ext4    defaults,noatime  0       1
		# a swapfile is not a swap partition, so no using swapon|off from here on, use  dphys-swapfile swap[on|off]  for that
		 
		/dev/md/snalraid0      /raid0    ext4    defaults    1 2
		 
		# mingwei's usb drive
		#/dev/sda1  /special_mingwei  ext3  defaults 1 2
		#/dev/sda2  /data_mingwei  ext3  defaults 1 2
		 
		# lumin's usb drive
		#/dev/sdb1  /special ext3  defaults 1 2
		#/dev/sdb2  /data ext3  defaults 1 2

To make raid1 array, we stopped the raid by ```mdadm --stop /dev/md/snalraid0```,
and then we use ```mdadm --create --verbose /dev/md/snalraid1 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1```
to make the raid1

Result from ```cat /proc/mdstat```

		cat /proc/mdstat
		Personalities : [raid0] [raid1]
		md127 : active raid1 sdb1[1] sda1[0]
		      7569152 blocks super 1.2 [2/2] [UU]
		      [>....................]  resync =  1.0% (81216/7569152) finish=15.3min speed=8121K/sec
		 
		unused devices: <none>

Result from ```mdadm --detail --scan```

		ARRAY /dev/md/snalraid1 metadata=1.2 name=duckpi02:snalraid1 UUID=e7aac48a:7611ae97:7509f9d3:000d4457