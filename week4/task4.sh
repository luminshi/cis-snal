#!/bin/bash

find / -type f -size +${1}c -exec ls -l {} \; | awk '{ print $9 "\t" $5 }' > task5out.txt
