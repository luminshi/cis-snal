#Week 4 Exercises

##1.
Create user accounts on your pi for everybody in the class. Collect and share a list of usernames during class. This means that your Pi should be accessible as much as possible during the week (share IP addresses or other specific info required for ssh-ing into the Pi you "own" with your "users"). Do NOT email passwords.

* We have a shared `Google Doc` file which includes everyone's public key and preferred username
* We then copied the usernames and keys from a shared [Google Spreasheet](https://docs.google.com/spreadsheets/d/1qmY3vcf68yb9L2VM9TPjpUtoQlgRoXHiS0Jhvv0oIWw/edit#gid=0)
* We then made a bash script that can read the tuple from the text file and create an account based on that information (create user and add public key to `.ssh` folder). The script is as follow:

		#!/bin/bash

		while IFS='=' read USER KEY
		do
			if [ ! -d "/home/$USER" ]; then
				sudo useradd -m $USER -g snal
				echo created user $USER
				sudo su - $USER -c "mkdir /home/$USER/.ssh; echo $KEY > /home/$USER/.ssh/authorized_keys; chmod 600 /home/$USER/.ssh/authorized_keys"
			else
				echo $USER already created
			fi
		done < credentials.txt


##2.
Write a script to check who has logged in into your Pi in the past month and log this information (usernames and last login) to a file.

* We made the script as follow:

		#!/bin/bash

		onemonth=`date --date="1 month ago" +"%y%m%d"`
		last -F -R| head -n -2| cut -c1-9,27-46 | grep -v "reboot"| while read line; do date=`date -d "$(echo $line | awk '{ print $2" "$3" "$4" "$5 }')" +"%y%m%d"`; [[ $date -ge "$onemonth" ]] && echo $line; done > output.txt
		
* We use `date` for the date range check in bash, and it is quite convenience in this case.

* Here is the sample out put:

	![task2-user-logged-in](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week4/task2.png)


##3.
Write a short bash script that does some logging of system state. Feel free to vary what the script monitors. For example, you can check system load and number of users at regular intervals and log this information in a file (other options are disk usage, e.g., to detect when nearly full, too many failed login attempts, and network connectivity). You can make your script a daemon, or have it be a cron job. Describe what you intended to do in your report and of course add the script to the repo.

* For the task, my intent is to write a simple bash script that will log the number of current logged in users and timestamp to a file. I will also create a cron job that will run this script every 5 minutes.

* I have the script as follow:

		#!/bin/bash

		log="logged in user #: "`users | wc -w`", "`date`

		echo $log>> /home/lshi/log.txt
		
* and the cron job description as follow:

	`*/5 * * * * /home/lshi/task3.sh`
	
* Sample output:

	![task3](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week4/task3.png)
	


##4.
Write a shell script that takes as input a file size (in bytes) and creates a text file containing all the files in the system that are larger than that size, sorted by size. Each line should contain the full path name of the file, followed by a tab, followed by the size in bytes.

* This task is relatively easy since all I have to do is use `find` to find all the files that is larger than user's input value: `find / -type f -size +${1}c`
* Here is the complete script:

		#!/bin/bash

		find / -type f -size +${1}c -exec ls -l {} \; | awk '{ print $9 "\t" $5 }' > task5out.txt


##5.
Graduate students only (optional for undergrads): write a script (either daemon or cron job) to check for intrusion similar to what happened to the Pis that got hacked last week.

* It is relatively hard for us to check the status since our PI was not hacked . However, if we were got hacked, I will be doing the following steps to exam what happened:

    * `grep` all the failed attempts from the `auth.log`
    * For each failed attempts, `grep` the IP again to see if there's any successful attemps.
    * If there were hacker got in, we can start to look into that particular time block, and check other logs to see what's going on.

