;
; BIND data file for local loopback interface
;
$TTL	604800
@	IN	SOA	ns1.lumin.com. admin.lumin.com. (
	          6		; Serial
	     604800		; Refresh
	      86400		; Retry
	    2419200		; Expire
	     604800 )	; Negative Cache TTL
;
; name servers - NS records
lumin.com. IN NS ns1.lumin.com.

ns1	IN	A	192.168.0.105

www	IN	A	192.168.0.105
