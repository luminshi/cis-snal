#Week 8 Exercises

I use serveral virtual machines to do all the exercises. I add host-only interface to each vm as well.

##Exercise 1:

* First, I have use two vms, one as DHCP server (#1), the other as the host.

* DHCP server (#1) has the following interface: ![](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week8/ex1-dhcp1-eth1.png)

* I disable the virtualbox's default DHCP server by running the cmd: `VBoxManage dhcpserver remove --netname HostInterfaceNetworking-vboxnet0`

* I then flush host's IP by `ip addr flush dev eth1`. Host information for eth1 as follow:
![](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week8/ex1-host-eth1.png)

* `isc-dhcp-server` is installed on the DHCP server (#1) after the steps above, and I modified the content of `/etc/dhcp/dhcpd.conf` as follow:

        subnet 192.168.0.0 netmask 255.255.255.0 {
          range 192.168.0.150 192.168.0.180;
          default-lease-time 600;
          max-lease-time 7200;
        }

* Restart the `isc-dhcp-server` and the DHCP server (#1) is good by now.

* On the host, run `dhclient -v eth1` to request an IP for interface `eth1`. Sample result:
![](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week8/ex1-host-dhclient.png)

* On the DHCP server (#1), I use `tcpdump -i eth1 port 67 or port 68 -v ` to capture all the DHCP requests incoming from `eth1` and the output as follow:

        22:14:54.084458 IP (tos 0x10, ttl 128, id 0, offset 0, flags [none], proto UDP (17), length 328)
            0.0.0.0.bootpc > 255.255.255.255.bootps: BOOTP/DHCP, Request from 08:00:27:9d:68:0d (oui Unknown), length 300, xid 0x1ca59f50, Flags [none]
              Client-Ethernet-Address 08:00:27:9d:68:0d (oui Unknown)
              Vendor-rfc1048 Extensions
                Magic Cookie 0x63825363
                DHCP-Message Option 53, length 1: Request
                Requested-IP Option 50, length 4: 192.168.0.150
                Hostname Option 12, length 9: "precise32"
                Parameter-Request Option 55, length 13: 
                  Subnet-Mask, BR, Time-Zone, Default-Gateway
                  Domain-Name, Domain-Name-Server, Option 119, Hostname
                  Netbios-Name-Server, Netbios-Scope, MTU, Classless-Static-Route
                  NTP
        22:14:54.098955 IP (tos 0x10, ttl 128, id 0, offset 0, flags [none], proto UDP (17), length 328)
            192.168.0.102.bootps > 192.168.0.150.bootpc: BOOTP/DHCP, Reply, length 300, xid 0x1ca59f50, Flags [none]
              Your-IP 192.168.0.150
              Server-IP 192.168.0.102
              Client-Ethernet-Address 08:00:27:9d:68:0d (oui Unknown)
              Vendor-rfc1048 Extensions
                Magic Cookie 0x63825363
                DHCP-Message Option 53, length 1: ACK
                Server-ID Option 54, length 4: 192.168.0.102
                Lease-Time Option 51, length 4: 600
                Subnet-Mask Option 1, length 4: 255.255.255.0
                Domain-Name Option 15, length 11: "example.org"

* We see there are two packets being captured by `tcpdump`. To answer the questions in the project description:
    * DHCP requests are not part of ICMP message.
    * MAC address of the DHCP server is not shown in the log, however, MAC address for the host is `08:00:27:9d:68:0d`.
    * The IP provided by DHCP server is `192.168.0.150`.

* I now have the second DHCP server (#2) setup and running, I captured logs at the client side using `tcpdump`, here is the thing I found about the DHCP:
    * Client first send out request to the whole network with IP:0.0.0.0
    * DHCP servers will echo offers back to the client.
    * Client will take the first offer from the network and set the IP address from the offer.
    * For example, the IP address of DHCP server (#2) is `192.168.0.3`. When I run `dhclient` on the client I get following output, the client took the offer from DHCP server #2:

            Listening on LPF/eth1/08:00:27:9d:68:0d
            Sending on   LPF/eth1/08:00:27:9d:68:0d
            Sending on   Socket/fallback
            DHCPREQUEST of 192.168.0.150 on eth1 to 255.255.255.255 port 67
            DHCPACK of 192.168.0.150 from 192.168.0.103
            bound to 192.168.0.150 -- renewal in 235 seconds.

* In order to solve the DHCP conflict issue, one can configure each server's `/etc/dhcpd.conf` file, and change the `subnet` block so they can manage a different set of IPs.


##Exercise 2:
The problem we have with the virtual machine is as follow:
* We can change host's virtual MAC address with no problem by using `macchanger`.
* We run `dhclient -eth1` to request an IP in the network, and DHCP servers receive the request as well.
* However, the host cannot receive any offer from the DHCP servers even though they sent the offers.
* I best guess is that virtualbox's virtual switch does not register the new MAC address on the host, therefore, the switch simply don't know where to forward the packet.
* The script itself is easy, one can simply write a inifite while loop in Bash. Only two statement is required in this case: 
    * Use `macchanger -p eth1` to get a new MAC address.
    * Use 'dhclient -v eth1' to require a new IP from DHCP server.

##Exercise 3:
* To configure a Ubuntu VM as a DNS server, start by installing the `bind9` pacakge.
* Then, it is important to chagen the host's nameserver to the IP address of the DNS's IP
    * DNS VM's private network IP is `192.168.0.105` therefore I change the `/etc/resolv.conf` file on the host to DNS VM's IP.
* I use `wget` to fetch a page from `google.com` a it works.
* I added a fake domain `lumin.com` (both A and PTR entry) to the `named.conf.local` file and added corresponding zone file as well. Check out my repo to see the files.
* I ran the `dig` cmd to view more details about `lumin.com` I created:
![](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week8/ex3.png)

##Exercise 4:
* I compiled and installed `dnsperf` on the host and changed the `forwarders` option in `named.conf.options` to `128.223.60.23`, which points to UO's DNS server.
* I downloaded the `url.txt` from class website and use `dnsperf` to run the file with flag `-d` and `-v`
* After two rounds, I get two output files and the first one costs around 4 seconds, the second one costs around 2 seconds.
* Two CDF graphs as follow:
    * Result 1:
        ![](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week8/ex4-result1.png)
    * Result 2:
        ![](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week8/ex4-result2.png)