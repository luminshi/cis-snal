#Week 6 Exercises

#1. 
*What packages are installed on the linux machines you have access to (ix, the Pi, your VM)? In your report, just describe how you found out and how many packages you saw (don't include a full list!)*

You can count the installed packages by `dpkg -l | wc -l`. `dpkg -l` will list all the packages, and `wc -l` will count how many lines of the output.

Here is the result on our Pi:

![step1](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step1.png)



#2. 
Install the Google Chrome browser on your VM. Include the commands you used in your report.

* First, I downloaded the Chrome's deb package as my VM is a Ubuntu dist.
* The following screenshot shows the command I used to install the Chrome, `sudo dpkg -i google-chrome-stable_current_amd64.deb`:

![step2](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step2.png)

* One thing to remember, since I had Chrome on my VM before, the process is just one command. However, for the first time installation, you might need to use `apt-get -f install` to force the installation.

#3. 
Install some new useful (to you) packages. Some ideas -- editors, different version of the compiler, a game, browser, etc. Describe how you found and installed them. Next, create your own package on your Ubuntu VM, following these instructions. Feel free to package your own program instead of the provided hello-2.7.tar.gz. You don't have to upload your package (last part of Section 6.4, bzr push and thereafter and Sec. 6.5). Instead, just commit the files to the week6/ directory in your bitbucket repository.

* My VM already installed everything I need for my tasks. It would be rather boring to remove the packages I installed and reinstall them again.
* You can always install the packages that is existed in `APT` repo. And on my VM, I have installed `VIM`, `git`, `nodejs` from a third party repo (add ppa link to APT manager) and `pip`.
* I have followed the instructions to package the hello program in the tutorial, and have the `deb` file uploaded to the bitbucket.

#4. 
What kernel modules are loaded on your Pi and VM? Pick one and tell me what it is for (in your report). Next unload one of the sound modules and reload it (show commands and output).

In Ubuntu, you can list modules by using `lsmod` tool.

##PI:
* here is the list of modules for PI:

![step4-pi](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step4-pi.png)

* To unload snd_bcm2835 module, I ran `sudo rmmod snd_bcm2835`

![step4-pi-unload](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step4-pi-unload.png)

* To load the module back, I ran `sudo modprobe snd_bcm2835`

##VM:
* I only show partial modules that are loaded on my VM:

![step4-vm](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step4-VM.png)


#5.
Graduate students only (optional for undergrads): Follow the directions here, appropriately modifying them to match your kernel version. Note that the

* I used my VM to build the `hello` module as seen on the ubuntu tutorial. And here is the result of the building process:

![step5](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step5.png)

#6.
(Optional for everyone, but +10 extra credit if you do this). First, check and report your current kernel version. Next, choose either your VM or the Pi and compile a new kernel from source code (I am not asking you to actually change the kernel, but if you are feeling adventurous and know what to do, please go ahead!). Test it by booting your VM or Pi. Show the output of uname -a upon a successful reboot. It is a good idea to back up your VM or Pi before doing this.


* I used `uname -a` to check my current kernel version, and it's 3.13

		Linux lumin-vm-mint 3.13.0-37-generic #64-Ubuntu SMP Mon Sep 22 21:28:38 UTC 2014 x86_64 x86_64 x86_64 GNU/Linux

* I downloaded linux kernel `4.0.2` from kernel.org and I used `make defconfig` to set the build config file as default.
* Then I ran `time make -j4` to build the kernel, and here is the screenshot:

![step6](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week6/step6.png)

* Interesting fact, I tried to build the same kernel on our lab's rack machine with -j12, and it only shorten the time by 1 minute. I guess not all the cores were being used.

* I then install the newly compiled kernel with `sudo make modules_install install`, and updated the `grub` list.

* However, I was not able to boot the system up with the 4.0 kernel.