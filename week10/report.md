# Week 10

## 1. Determining the AS and Geographic Path

* For AS # finding, one can simply append -A flag to the traceroute program. The result as follows:

* For IP 184.50.85.91

		traceroute to 184.50.85.91 (184.50.85.91), 30 hops max, 60 byte packets
		1  vl-8.uonet1-gw.uoregon.edu (128.223.8.2) [AS3582]  0.948 ms  0.764 ms  0.811 ms
		2  vl-3.uonet9-gw.uoregon.edu (128.223.3.9) [AS3582]  0.328 ms  0.411 ms  0.403 ms
		3  vl-675.core0-gw.pdx.oregon-gigapop.net (198.32.163.217) [AS4600]  2.729 ms  2.763 ms  2.748 ms
		4  xe-9-2-0.692.sttl0.tr-cps.internet2.edu (198.71.47.205) [*]  108.213 ms  108.308 ms  108.303 ms
		5  xe-8-2-0.0.rtr.losa.net.internet2.edu (64.57.20.222) [*]  27.006 ms  27.228 ms  27.215 ms
		6  et-4-0-0.80.rtr.wilc.net.internet2.edu (64.57.20.127) [*]  26.974 ms  27.018 ms  26.989 ms
		7  * * *
		8  akamai.bundl3.br03.sin02.pccwbtn.net (63.218.107.206) [AS19710/AS3491]  186.541 ms TenGE0-2-0-14.br03.sin02.pccwbtn.net (63.218.228.178) [AS3491]  186.840 ms TenGE0-2-0-15.br03.sin02.pccwbtn.net (63.218.228.182) [AS3491]  186.686 ms
		9  akamai.bundl3.br03.sin02.pccwbtn.net (63.218.107.206) [AS19710/AS3491]  186.698 ms  186.744 ms  186.735 ms
		10  a184-50-85-91.deploy.static.akamaitechnologies.com (184.50.85.91) [AS20940]  186.671 ms  186.610 ms  186.333 ms

		GEO:
		AS3582: Eugene, Oregon
		AS4600: US
		198.71.47.205 [*]: Ann Arbor, Michigan
		64.57.20.222 [*]: Ann Arbor, Michigan
		64.57.20.127 [*]: Ann Arbor, Michigan
		AS19710/AS3491: Singapore
		AS3491: Herndon, Virginia
		AS20940: Cambridge, Massachusetts

* For IP 8.8.8.8

		traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
		1  vl-8.uonet1-gw.uoregon.edu (128.223.8.2) [AS3582]  18.751 ms  18.747 ms  18.781 ms
		2  vl-3.uonet9-gw.uoregon.edu (128.223.3.9) [AS3582]  0.316 ms  0.365 ms  0.350 ms
		3  eugn-car1-gw.nero.net (207.98.68.181) [AS3701]  0.252 ms  0.296 ms  0.282 ms
		4  eugn-core2-gw.nero.net (207.98.64.162) [AS3701]  3.549 ms  3.656 ms  3.651 ms
		5  ptck-core2-gw.nero.net (207.98.64.10) [AS3701]  3.454 ms  3.588 ms  3.581 ms
		6  * * *
		7  72.14.238.122 (72.14.238.122) [AS15169]  6.484 ms 72.14.239.148 (72.14.239.148) [AS15169]  7.113 ms 209.85.247.15 (209.85.247.15) [AS15169]  7.845 ms
		8  google-public-dns-a.google.com (8.8.8.8) [AS15169]  6.897 ms  7.696 ms  6.900 ms

		GEO:
		AS3582: Eugene, Oregon
		AS3701: Eugene, Oregon
		AS15169: Mountain View, California

* For IP 69.53.236.21

		traceroute to 69.53.236.21 (69.53.236.21), 30 hops max, 60 byte packets
		1  vl-8.uonet1-gw.uoregon.edu (128.223.8.2) [AS3582]  0.455 ms  0.476 ms  0.518 ms
		2  vl-3.uonet9-gw.uoregon.edu (128.223.3.9) [AS3582]  11.937 ms  11.963 ms  11.972 ms
		3  eugn-car1-gw.nero.net (207.98.68.181) [AS3701]  0.237 ms  0.233 ms  0.225 ms
		4  eugn-core1-gw.nero.net (207.98.64.161) [AS3701]  1.019 ms  1.015 ms  0.994 ms
		5  te-4-4.car1.Sacramento1.Level3.net (4.53.200.1) [AS3356]  10.480 ms  10.556 ms  10.566 ms
		6  * * *
		7  XO-COMMUNIC.car3.SanJose1.Level3.net (4.71.112.78) [AS3356]  14.089 ms  13.566 ms  13.811 ms
		8  216.156.84.6.ptr.us.xo.net (216.156.84.6) [AS2828]  14.248 ms  14.189 ms  14.206 ms
		9  xe-2-2-0-955.jnrt-edge02.prod1.netflix.com (69.53.225.30) [AS40027]  14.972 ms  14.907 ms  15.086 ms
		10  te1-8.csrt-agg02.prod1.netflix.com (69.53.225.10) [AS40027]  15.061 ms  14.775 ms  14.938 ms
		11  www.nflxext.com (69.53.236.21) [AS55095/AS2906/AS40027]  14.497 ms  14.482 ms  14.550 ms

		GEO:
		AS3582: Eugene, Oregon
		AS3701: Eugene, Oregon
		AS3356: US
		AS2828: US
		AS40027: Los Gatos, California

## End-to-End Bandwidth Measurement

* I used two servers in the rack room to test the end-to-end bandwitdh

* Open the iperf server mode on one machine by using ```iperf -s```
* And on the other server, I ran the client mode. The result as follows:

		lumin@panther:~$ iperf -c 128.223.8.85 -p 5001
		------------------------------------------------------------
		Client connecting to 128.223.8.85, TCP port 5001
		TCP window size: 85.0 KByte (default)
		------------------------------------------------------------
		[  3] local 128.223.8.86 port 35301 connected with 128.223.8.85 port 5001
		[ ID] Interval       Transfer     Bandwidth
		[  3]  0.0-10.0 sec  1.10 GBytes   943 Mbits/sec

## Receiver Bandwidth Measurement

* I used my desktop computer in the office, and opened a youtube video that is 6:50 long
* ```sudo tcpdump -i eth0 -l -e -n```
* Sample output looks like this:

		21:23:17.111631 00:50:56:e0:82:1b > 00:0c:29:9f:10:45, ethertype IPv4 (0x0800), length 74: 173.194.33.133.443 > 192.168.195.128.38335: UDP, length 32
		21:23:17.125707 00:50:56:e0:82:1b > 00:0c:29:9f:10:45, ethertype IPv4 (0x0800), length 142: 173.194.33.133.443 > 192.168.195.128.38335: UDP, length 100
		21:23:17.152292 00:0c:29:9f:10:45 > 00:50:56:e0:82:1b, ethertype IPv4 (0x0800), length 82: 192.168.195.128.38335 > 173.194.33.133.443: UDP, length 40

* Then you apply regex ```/ length (\d+):/``` for each line to get the length in byte and add length to a sum variable. With each time 10 seconds window, you can do an average on the sum to get the estimated bandwidth:

		21:28:19  438559.97 Bps
		21:28:30  227179.01 Bps
		21:28:40  231372.67 Bps
		21:28:51  220867.00 Bps
		21:29:02  262091.71 Bps
		21:29:12  236749.33 Bps
		21:29:22  265745.72 Bps
		21:29:32   35456.93 Bps
		21:29:42  240871.65 Bps
		21:29:55   57835.07 Bps
		21:30:05      59.31 Bps
		21:30:15     194.80 Bps
		21:30:26  181766.52 Bps
		21:30:37     206.75 Bps
		21:30:47   85463.10 Bps
		21:30:59     673.04 Bps
		21:31:10  180511.76 Bps
		21:31:20     977.30 Bps


* As you can see from the result, youtube video was not downloaded from the server directly, instead, it splits the video data into trunks for users to download, and the bandwitdh is fixed around 200 KB/s for 480p video


## Multiple Gateway Problem
* I am not quite sure about what the question is asking, but I am assuming it's asking why the machine connected to a home router cannot be accessed directly from outside?

* So for the first case, home router, there's no direct public IP for Pi since it is behind the NAT wall. To fix this, you need to manually map some port to the Pi's internal router IP

* UO wireless network actually provides the public IP, therefore, no need to do anything to access a machine connects to the UO wireless network


## ARP poisoning
* For this experiment, we borrowed an old switch from Paul
* We have 3 machines connected to a switch, and we manually assigned an IP for each machine

		Pi: 192.168.0.101
		Mac: 192.168.0.102
		Dell: 192.168.0.103

* We found ```arp``` can't change the arp tables in other machines, therefore we used the GUI software called ```ettercap``` to help making ARP poisoning msgs.
* The software basically make annoucement such that the machine (Pi, in our case) has the IP-mac mapping (Dell, in this case) and the switch will forward the packet to Pi instead of Dell.

* To detect such ARP poisoning is easy, one can run ```tcp``` dump to see whether someone is announcing traffic that should belong to itself.