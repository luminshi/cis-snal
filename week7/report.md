#Week 7 Exercises

#1.
Configuring network interfaces

* Apparently, `ifconfig` assgined IP is temporary.
* If a static IP is wanted, you need to change `etc/network/interfaces` and make sure dhcp is not enabled. For example: `iface eth0 inet static` will use disable the dhcp request.

#2.
Managing routing table of local computer

* To manually add a temp route, you can use `route` to do this.
* For example, you can easily add a routing entry that can go through the ethernet interface, when it matches the IP block you defined.
* Like this: `route add -net 111.111.111.0/21 gw yourEthIP dev eth0s`

#3.
Firewall configuration NAT/IPTables

* I have two virtual machines running on my desktop, and here are the IPs for them.
* VM0: 192.168.221.132
* VM1: 192.168.221.129

        VM0:
        eth0      Link encap:Ethernet  HWaddr 00:0c:29:80:10:5b  
                  inet addr:192.168.221.132  Bcast:192.168.221.255  Mask:255.255.255.0
                  inet6 addr: fe80::20c:29ff:fe80:105b/64 Scope:Link
                  UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                  RX packets:20684 errors:0 dropped:0 overruns:0 frame:0
                  TX packets:9783 errors:0 dropped:0 overruns:0 carrier:0
                  collisions:0 txqueuelen:1000 
                  RX bytes:23814609 (23.8 MB)  TX bytes:1451242 (1.4 MB)

        VM1:
        eth0      Link encap:Ethernet  HWaddr 00:0c:29:44:95:28  
              inet addr:192.168.221.129  Bcast:192.168.221.255  Mask:255.255.255.0
              inet6 addr: fe80::20c:29ff:fe44:9528/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:5680 errors:0 dropped:0 overruns:0 frame:0
              TX packets:2065 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:7323822 (7.3 MB)  TX bytes:179219 (179.2 KB)


1. To make a default blacklist, simply run this `iptables -P INPUT DROP`.
2. To allow all VM1's traffic to pass through, run this `iptables -A INPUT -s 192.168.221.129 -j ACCEPT`.
3. To log blocked traffic, I first flushed the `iptables` and set `INPUT` as `ACCEPT` so it will have default behaviors.
    * I then add a new chain, name it LOGGING: `iptables -N LOGGING`.
    * Add a selecting rule: `iptables -A INPUT -s 192.168.221.129 -j LOGGING`.
    * A few options for the new `LOGGING` chain: `iptables -A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables-Dropped: " --log-level 4`.
    * Finally, set everything forwarded to `LOGGING` to be dropped: `iptables -A LOGGING -j DROP`
    * Here's some sample log output from `/var/log/syslog`.
        ![LOGGING log](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week7/step3-0.PNG)
    * In order to remove the LOGGING chain, make sure all rules points to LOGGING are removed first. 

#4.
##Part one and two: configure linux machine as a router, and log something.

* I used `Vagrant` to initiate two vm instances. First one with two NICs. `eth0` connects to the outsite world and `eth0` is connected to private network. For the second, I only use the private network that has the connection to the first host.

* In order to remove the host2's network connection, I have to remove the default eth0 route. This is how it looks like originally.

    root@precise32:/home/vagrant# ip route show
    default via 10.0.2.2 dev eth0 
    default via 10.0.2.2 dev eth0  metric 100 
    10.0.2.0/24 dev eth0  proto kernel  scope link  src 10.0.2.15 
    192.168.33.0/24 dev eth1  proto kernel  scope link  src 192.168.33.11 

* I then ran `ip route del default` to delete the default route info.
* And this is what i got after I did the above cmd:

    root@precise32:/home/vagrant# ping google.com
    PING google.com (216.58.216.174) 56(84) bytes of data.
    ^C
    --- google.com ping statistics ---
    5 packets transmitted, 0 received, 100% packet loss, time 4025ms

* At host1, i have to enable the ipv4 forwarding option by `echo 1 > /proc/sys/net/ipv4/ip_forward`
* Then I ran following cmd's to configure the `iptables`
  
  ```
  iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
  iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
  iptables -N LOGGING
  iptables -A FORWARD -i eth1 -o eth0 -j LOGGING
  iptables -A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables-Router: " --log-level 4
  iptables -A LOGGING -j ACCEPT
  ```

* The above cmds basically will accepts incoming traffic to the host2, since the output is eth1, and I created a new `chain` called `LOGGING` that will match any packet from eth1 (where host2 comes from) and apply the policies in `LOGGING`

* This is the look of my `iptables` on host1:

```
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere             state RELATED,ESTABLISHED
LOGGING    all  --  anywhere             anywhere            

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         

Chain LOGGING (1 references)
target     prot opt source               destination         
LOG        all  --  anywhere             anywhere             limit: avg 2/min burst 5 LOG level warning prefix "IPTables-Router: "
ACCEPT     all  --  anywhere             anywhere            
```

* Here is some data from `/var/log/syslog`, I used `grep` to catch all `IPTables-Router` entries:

```
root@precise32:/home/vagrant# cat /var/log/syslog | grep "IPTables-Router"
May 16 06:40:02 precise32 kernel: [ 2309.829011] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.174 LEN=60 TOS=0x00 PREC=0x00 TTL=63 ID=60423 DF PROTO=TCP SPT=60650 DPT=80 WINDOW=14600 RES=0x00 SYN URGP=0 
May 16 06:40:02 precise32 kernel: [ 2309.897396] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.174 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=60424 DF PROTO=TCP SPT=60650 DPT=80 WINDOW=14600 RES=0x00 ACK URGP=0 
May 16 06:40:02 precise32 kernel: [ 2309.898387] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.174 LEN=150 TOS=0x00 PREC=0x00 TTL=63 ID=60425 DF PROTO=TCP SPT=60650 DPT=80 WINDOW=14600 RES=0x00 ACK PSH URGP=0 
May 16 06:40:02 precise32 kernel: [ 2310.023075] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.174 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=60426 DF PROTO=TCP SPT=60650 DPT=80 WINDOW=15471 RES=0x00 ACK URGP=0 
May 16 06:40:02 precise32 kernel: [ 2310.034694] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.164 LEN=60 TOS=0x00 PREC=0x00 TTL=63 ID=15147 DF PROTO=TCP SPT=56246 DPT=80 WINDOW=14600 RES=0x00 SYN URGP=0 
May 16 06:57:26 precise32 kernel: [ 3353.305829] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.164 LEN=60 TOS=0x00 PREC=0x00 TTL=63 ID=7539 DF PROTO=TCP SPT=34231 DPT=443 WINDOW=14600 RES=0x00 SYN URGP=0 
May 16 06:57:26 precise32 kernel: [ 3353.370490] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.164 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=7540 DF PROTO=TCP SPT=34231 DPT=443 WINDOW=14600 RES=0x00 ACK URGP=0 
May 16 06:57:26 precise32 kernel: [ 3353.371391] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.164 LEN=266 TOS=0x00 PREC=0x00 TTL=63 ID=7541 DF PROTO=TCP SPT=34231 DPT=443 WINDOW=14600 RES=0x00 ACK PSH URGP=0 
May 16 06:57:26 precise32 kernel: [ 3353.433810] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.164 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=7542 DF PROTO=TCP SPT=34231 DPT=443 WINDOW=17016 RES=0x00 ACK URGP=0 
May 16 06:57:26 precise32 kernel: [ 3353.434043] IPTables-Router: IN=eth1 OUT=eth0 MAC=08:00:27:d9:94:41:08:00:27:f0:8e:07:08:00 SRC=192.168.33.11 DST=216.58.216.164 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=7543 DF PROTO=TCP SPT=34231 DPT=443 WINDOW=19880 RES=0x00 ACK URGP=0 
```

* From here, I can do more `grep` calls to get the destPort and srcIP, and I can use these info to build a chart.