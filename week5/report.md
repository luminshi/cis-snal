#Week 5 Exercises

##1. 
System preparation: check the current process limit settings. Change the process limits of regular users on your Pi to 100. This should take a few minutes. Briefly mention what you did in your report. Make sure to do this in the first few minutes of class (or earlier).

Once I've logged into our pi, I ran `ulimit -u` to check the current limit of my account's process limit. Since my team member Mingwei already did this step. My ouput for process limit is `100` already.

Still, if I want to change the process limit. I will simply edit the file located at `/etc/security/limits.conf` and set `hard` `nproc` to 100. 

Configuration file is as follow, we have global `nproc` set to 100:

![step1](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week5/step1.png)


##2.
This must be done in class. Make sure everyone has completed problem 1. Pick a victim Pi (not yours) and log in as a regular user.

* Try to bring it down, e.g., by using a [fork bomb](http://en.wikipedia.org/wiki/Fork_bomb).
	* Someone put down the `brix` server during the class, therefore we can't access all PIs via `brix` server.
	* Here is the bash version of fork bomb:
		
			bomb() {
			  bomb | bomb &
			};
			bomb
	
	* Basically it is a recursive function call that create background process infinitely.

* Check if your Pi is being attacked and by whom. Do you observe any problems? How did you deal with them (use sudo if you need to change other users' processes or files)?
	* Adam tried to use fork bomb to bring down the our PI, however, the `nproc` limit is set to 100, and that won't cause a big issue in this case.
	* Carl group made a c program that will allocate memory in a infinite while loop, that took up all the pi's memory and cause its death.

##3.
Write a daemon (bash or any other language you prefer) that estimates what percentage of time your Pi was up and accessible over the network. Do this as early as possible. Note that just calling `uptime` is not quite sufficient and you may need to run scripts on other machines. Describe how you did this in your report. Make the automatically generated results available on a regular basis at some URL (e.g., in your ~/public_html space on ix) and post the URL on Piazza as soon as it's ready. For example, a basic plain text log file with regular periods, e.g., one for each hour and the % availability during each period would be fine, but the format is entirely up to you. Summary statistics would also be nice (e.g., per day and per week). The owner(s) of the Pi that was most accessible will win a surprise.

I made a simple python based HTTP RESTful service by using `FlaskRESTful` library. It has 3 simple functions:

* Show log
* Punch pi02's up status
* Punch pi02's down status

Then I made a simple bash script that will constantly `ping` PI02's address. If the program exit code is `0`, that means Pi is currently running and it will send a HTTP request to my log server to keep a up time record. Otherwise, a down record will be recorded.

Here is my sample bash script:

		#!/bin/bash

		while true; do
			ping -c 1 snalpi.duckdns.org > /dev/null;
			if [ $? -eq 0 ]; then
				echo "good"
				curl panther.cs.uoregon.edu:7878/good
			else
				echo "nogood"
				curl panther.cs.uoregon.edu:7878/nogood
			fi
			sleep 300
		done

I will include the python script and the bash script to my week5 folder on bitbucket as well.

Sample log screenshot:

![log](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week5/step3.png)



##4.
Log into other students' Pis and create various problems -- circular symbolic links, deleted open files, a daemon-like program that makes a lot of directories or files, creating hidden directories (e.g., named ...), etc. Be creative. Do NOT tell the Pi owners what you did. Do this on at least four other Pis (not your own). Write scripts for at least some of your mischief, so that you can easily replicate it on several machines. Note in your report what you did and to which Pis. Also let me know on Piazza which Pis you targeted (post private to instructors only!).

Since our PIs has the disk formatted as `ext4` as its file system. It is really easy to do some damage if the owner does not set the hard limit on how much space a user can create by using the `fallocate` tool.

Using the following command, I was able to create a huge file instantly unlikey other tools such as `dd`:

`fallocate -l 15G .bigfile`

Here is a screenshot of what I did to `duckpi03`:

![step4](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week5/step4.png)

I was able to access other PIs at the moment.

##5.
On your own Pi, write scripts to detect as many problems as you can and fix them automatically if possible or at least report them (e.g., to standard out). You should run these with sudo or as root. On Monday, May 4, we will go through the reports to see what problems on your Pi you were able to find and which ones you missed.

Rather to write magic scripts to solve all the problems, I think it will be better to check the system log manually and to see if anything is wrong with the system, since, we created accounts for everyone to access.

We found couple problem on our PI. And two of them were using `dd` to dump random data to the user space. We simply removed all random files in this case.

We then set couple hard limits on the `snal` user group, which is everyone's (not including us) primary group. Here is the config settings:

		@snal   hard    as      10000
		@snal   hard    memlock 10000
		@snal   hard    rss     10000
		@snal   hard    stack   10000
		@snal   hard    cpu             1
		@snal   hard    maxlogins       3
		@snal   hard    fsize   10000
		@snal   hard    nofile  10
		@snal   hard    locks   10
		@snal   hard    data    10000
		
We set the cpu limit due to the other attempt that cause the PI to use 100% of its cpu time.

#6.
Graduate students only (optional for undergrads): Extend problem 3 to alert you if any problems are detected -- e.g., Pi can't be reached, or the load is over some threshold, etc.

I extended my python script a little so that every our Pi fails, it will send an email to me by using the `mailgun.org` service.

Sample output:

![step6](https://dl.dropboxusercontent.com/u/21440899/snal-screenshots/week5/step6.png)


I have attached all my script to the bitbucket repo.