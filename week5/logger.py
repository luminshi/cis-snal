import sqlite3 as lite
import os
import sys
from flask import Flask
from flask.ext import restful

app = Flask(__name__)
api = restful.Api(app)

def insert(status):
    con = None
    try:
        con = lite.connect('test.db')
        cur = con.cursor()
        cur.execute("INSERT INTO Log(Status, Time) VALUES ("+str(status)+", datetime());")
        con.commit()

    except lite.Error, e:
        if con:
            con.rollback()

        print "Error %s:" % e.args[0]
        sys.exit(1)

    finally:
        if con:
            con.close()

class printLog(restful.Resource):
    def get(self):
        con = None
        uprate = 0;
        try:
            con = lite.connect('test.db')
            cur = con.cursor()
            cur.execute("SELECT * FROM Log")
            allRows = cur.fetchall()

            cur.execute("SELECT * FROM Log Where Status = '%d'" % 1)
            goodRows = cur.fetchall()

            uprate = float(len(goodRows)) / float(len(allRows))

        except lite.Error, e:
            if con:
                con.rollback()

            print "Error %s:" % e.args[0]
            sys.exit(1)

        finally:
            if con:
                con.close()

        return {'duckpi02 up rate: ': uprate, 'success pings vs. all pings': str(len(goodRows))+"/"+str(len(allRows))}

class logSuccess(restful.Resource):
    def get(self):
        insert(1)
        return {'status': 'success'}

class logFailure(restful.Resource):
    def get(self):
        insert(0)
        cmd = './ping.sh'
        os.system(cmd)
        return {'status': 'failed'}

api.add_resource(printLog, '/log')
api.add_resource(logSuccess, '/good')
api.add_resource(logFailure, '/nogood')

if __name__ == '__main__':
    app.run(port=7878, host='0.0.0.0')
