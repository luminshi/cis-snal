#!/bin/bash

while true; do
        ping -c 1 snalpi.duckdns.org > /dev/null;
        if [ $? -eq 0 ]; then
                echo "good"
                curl panther.cs.uoregon.edu:7878/good
        else
                echo "nogood"
                curl panther.cs.uoregon.edu:7878/nogood
        fi
        sleep 300
done
